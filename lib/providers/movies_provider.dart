import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:prueba_plenna/helpers/debouncer.dart';
import 'package:prueba_plenna/models/models.dart';

class MoviesProvider extends ChangeNotifier{
  String _baseUrl = 'api.themoviedb.org';
  String _apiKey = 'dcdb627ba0b290d7706915515fcd7382';
  String _languaje = 'es-ES';

  List<Movie> onDisplayMovies =[];
  List<Movie> popularMovies =[];
  Map<int,List<Cast>> movieCast ={};
  int _popularPage = 0;

  final debouncer = Debouncer(duration: Duration(milliseconds: 500));
  final StreamController<List<Movie>> _suggestionStramController = new StreamController();
  Stream<List<Movie>> get suggestionsStream => this._suggestionStramController.stream;

  MoviesProvider(){
    print('MoviesProvider inicializado');
    this.getOnDisplayMovies();
    this.getPopularMovies();
  }

  Future<String> _getJsonData(String endpoint, [int page = 1]) async{
    final url = Uri.https(this._baseUrl, endpoint,{
      'api_key': _apiKey,
      'language': _languaje,
      'page': '$page'
    });
    final response = await http.get(url);
    return response.body;
  }

  getOnDisplayMovies() async{
    final jsonData = await this._getJsonData("3/movie/now_playing");
    final nowPlayingRespose = NowPlayingResponse.fromJson(jsonData);

    onDisplayMovies = nowPlayingRespose.results;
    notifyListeners();
  }

  getPopularMovies() async{
    _popularPage++;
    final jsonData = await this._getJsonData("3/movie/popular", _popularPage);
    final popularRespose = PopularResponse.fromJson(jsonData);

    popularMovies = [... popularMovies, ...popularRespose.results];
    notifyListeners();
  }

  Future<List<Cast>> getMovieCast(int movieId) async{
    if (movieCast.containsKey(movieId)) return movieCast[movieId]!;

    final jsonData = await this._getJsonData("3/movie/$movieId/credits");
    final creditsRespose = CreditsResponse.fromJson(jsonData);
    movieCast[movieId] = creditsRespose.cast;
    return creditsRespose.cast;
  }

  Future<List<Movie>> seartchMovie(String query) async{
    final url = Uri.https( _baseUrl, '3/search/movie', {
      'api_key': _apiKey,
      'language': _languaje,
      'query': query
    });

    final response = await http.get(url);
    final searchResponse = SearchResponse.fromJson( response.body );

    return searchResponse.results;
  }

  void getSuggestionsByQuery(String searchTerm){
    debouncer.value = '';
    debouncer.onValue = (value) async{
      final results = await this.seartchMovie(value);
      this._suggestionStramController.add(results);
    };
    final timer = Timer.periodic(Duration(milliseconds: 300), (_) { 
      debouncer.value = searchTerm;
    });
    Future.delayed(Duration(milliseconds: 301)).then((_) => timer.cancel());
  }
}