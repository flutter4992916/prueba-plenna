export 'package:prueba_plenna/models/search_response.dart';
export 'package:prueba_plenna/models/credits_response.dart';
export 'package:prueba_plenna/models/popular_response.dart';
export 'package:prueba_plenna/models/movie.dart';
export 'package:prueba_plenna/models/now_playing_response.dart';
