import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:prueba_plenna/providers/movies_provider.dart';
import 'package:prueba_plenna/search/search_delegate.dart';
import 'package:prueba_plenna/widgets/widgets.dart';

class HomeScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final moviesProvider = Provider.of<MoviesProvider>(context);
    print(moviesProvider.onDisplayMovies);
    return Scaffold(
      appBar: AppBar(
        title: Text('Películas en cines'),
        elevation: 0,
        actions: [
          IconButton(
            icon: Icon( Icons.search_outlined ),
            onPressed: () => showSearch(
                context: context,
                delegate: MovieSearchDelegate()
            )
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            //Tarjetas principales
            CardSwiper(movies: moviesProvider.onDisplayMovies),
            //Slider de peliculas
            MovieSlider(
                movies:moviesProvider.popularMovies,
                title:'Populares',
                onNextPage: () => moviesProvider.getPopularMovies()),
          ],
        ),
      )
    );
  }
}